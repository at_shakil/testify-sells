1.upto(3) do |i|
  txt = "Template #{i}"
  CaseStudyTemplate.create(
     title: txt,
     preview: File.open(File.join('seeds', 'templates', "case_study_preview_#{i}.png")),
     template: File.read(File.join('seeds', 'templates', "case_study_#{i}.html"))
  )
end



1.upto(3) do |i|
  txt = "Style #{i}"
  TileTemplate.create(
     title: txt,
     preview: File.open(File.join('seeds', 'templates', "tile_preview_#{i}.png")),
     template: File.read(File.join('seeds', 'templates', "tile_#{i}.html"))
  )
end


1.upto(4) do |i|
  txt = "Style #{i}"
  TestimonialTemplate.create(
    title: txt,
    preview: File.open(File.join('seeds', 'templates', "testimonial_preview_#{i}.png")),
    template: File.read(File.join('seeds', 'templates', "testimonial_#{i}.html"))
  )
end
